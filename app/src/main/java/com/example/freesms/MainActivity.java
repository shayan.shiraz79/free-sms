package com.example.freesms;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    EditText edtsender,edtPhone,edtmassage,edtApi;
    Button btnSend;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtApi=(EditText)findViewById(R.id.api);
        edtPhone=(EditText)findViewById(R.id.phone);
        edtsender=(EditText)findViewById(R.id.sender);
        edtmassage=(EditText)findViewById(R.id.message);
        btnSend=(Button)findViewById(R.id.btn_send);
        edtApi.setText("NDE2ODQxNDk0MzMxNDUzOTMzMzI1NDQ3NzA0MzQ1NzY=");
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // Construct data
                    String apiKey = "apikey=" + edtApi;
                    String message = "&message=" + edtmassage.getText().toString();
                    String sender = "&sender=" + edtsender.getText().toString();
                    String numbers = "&numbers=" + edtPhone.getText().toString();

                    // Send data
                    HttpURLConnection conn = (HttpURLConnection) new URL("https://api.txtlocal.com/send/?").openConnection();
                    String data = apiKey + numbers + message + sender;
                    conn.setDoOutput(true);
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
                    conn.getOutputStream().write(data.getBytes("UTF-8"));
                    final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    final StringBuffer stringBuffer = new StringBuffer();
                    String line;
                    while ((line = rd.readLine()) != null) {
                        //stringBuffer.append(line);
                    }
                    rd.close();

                  //  return stringBuffer.toString();
                } catch (Exception e) {
                    System.out.println("Error SMS "+e);
                   // return "Error "+e;
                }
            }
        });

        StrictMode.ThreadPolicy policy=new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

    }
}